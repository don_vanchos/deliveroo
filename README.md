# Deliveroo task

## Stack

TypeScript

React + create-react-app

Sass + BEM

JEST

## Available Scripts

In the project directory, you can run:

### `npm start`

### `npm test`

### `npm run build`

### `npm run eject`

### `npm run analyze`

## About me

Ivan Ergunov
hozblok@gmail.com
https://github.com/hozblok
https://stackoverflow.com/users/5717886/don-vanchos
