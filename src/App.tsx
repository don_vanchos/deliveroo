import { Component } from 'react';

import MainPage from './blocks/MainPage/MainPage';
import { MobileContext } from './providers/mobile/MobileContext';
import MobileProvider from './providers/mobile/MobileProvider';
import { ThemeProvider } from './providers/theme/ThemeProvider';

const MIN_DESKTOP_RESOLUTION = 768;

class App extends Component {
    static contextType = MobileContext;

    handleWindowSizeChange = () => {
        this.context.setMobile(window.innerWidth <= MIN_DESKTOP_RESOLUTION);
    };

    componentDidMount() {
        window.addEventListener('resize', this.handleWindowSizeChange);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleWindowSizeChange);
    }

    render() {
        return <MainPage />;
    }
}

export default function AppWrapper() {
    // TODO save theme somewhere in local storage...
    return (
        <ThemeProvider theme="light">
            <MobileProvider mobile={window.innerWidth <= MIN_DESKTOP_RESOLUTION}>
                <App />
            </MobileProvider>
        </ThemeProvider>
    );
}
