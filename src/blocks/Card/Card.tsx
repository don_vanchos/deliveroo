import './Card.scss';

import { Component } from 'react';

import { cn } from '@bem-react/classname';

const block = cn('Card');

export interface ICard {
    /**
     * Url for image.
     * TODO: support images from assets?
     */
    imgSrc: string;
    /** Use object-fit: contain with 100% width for the image. */
    imgFit?: boolean;
    imgAlt?: string;
    imgTooltip?: string;
    title: string;
    subtitle?: string;
}

export default class Card extends Component<ICard> {
    render() {
        const { title, imgFit, imgSrc, imgAlt, subtitle, imgTooltip } = this.props;
        return (
            <div className={block()}>
                <img src={imgSrc} alt={imgAlt} title={imgTooltip} className={block('Img', { fit: !!imgFit })} />
                <div>
                    <div title={title} className={block('Title')}>
                        {title}
                    </div>
                    {subtitle != null && (
                        <div title={subtitle} className={block('Subtitle')}>
                            {subtitle}
                        </div>
                    )}
                </div>
            </div>
        );
    }
}
