import './MainPage.scss';

import { Component } from 'react';

import Header from '../Header/Header';
import Restaurants from '../Restaurants/Restaurants';

export default class MainPage extends Component {
    render() {
        return (
            <div className="MainPage">
                <Header user="Jane Smith" />
                <Restaurants />
            </div>
        );
    }
}
