import './Logo.scss';

import { ReactComponent as LogoSvg } from '../../assets/svgs/logo-horizontal.svg';

export default function Logo() {
    return <LogoSvg className="Logo" />;
}
