import './Header.scss';

import { useContext } from 'react';

import { cn } from '@bem-react/classname';

import { MobileContext } from '../../providers/mobile/MobileContext';
import Logo from '../Logo/Logo';
import UserBadge from '../UserBadge/UserBadge';

const block = cn('Header');

export interface IHeader {
    user: string;
}

export default function Header(props: IHeader) {
    const { mobile } = useContext(MobileContext);
    return (
        <div className={block({ mobile })}>
            <Logo />
            <div className={block('User', { mobile })}>
                <UserBadge />
                {!mobile && <div className={block('Name')}>{props.user}</div>}
            </div>
        </div>
    );
}
