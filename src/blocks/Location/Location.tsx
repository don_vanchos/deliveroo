import './Location.scss';

import { useContext } from 'react';

import { cn } from '@bem-react/classname';

import { MobileContext } from '../../providers/mobile/MobileContext';
import { ThemeContext } from '../../providers/theme/ThemeContext';
import Button from '../Button/Button';

const block = cn('Location');

export default function Location(props: { title: string }) {
    const { theme, setTheme } = useContext(ThemeContext);
    const { mobile } = useContext(MobileContext);
    return (
        <div className={block({ mobile })}>
            <div>
                <div className={block('Description')}>Location</div>
                <div className={block('Title')}>{props.title}</div>
            </div>
            <Button
                mode="accent"
                onClick={() => {
                    // TODO: delete me and implement this handler.
                    console.error('Change location button: ACTION NOT IMPLEMENTED!');
                    setTheme(theme === 'light' ? 'dark' : 'light');
                }}
            >
                Change location
            </Button>
        </div>
    );
}
