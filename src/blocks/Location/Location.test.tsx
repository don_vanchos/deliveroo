import React from 'react';

import { render, screen } from '@testing-library/react';

import Location from './Location';

describe('Location', () => {
    it('should render "Change location" button', () => {
        render(<Location title="Test" />);
        const linkElement = screen.getByText(/Change location/i);
        expect(linkElement).toBeInTheDocument();
    });
});
