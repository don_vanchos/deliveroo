import './CardsContainer.scss';

import { Component, ReactNode } from 'react';

import { cn } from '@bem-react/classname';

import { MobileContext } from '../../providers/mobile/MobileContext';
import Card from '../Card/Card';

const block = cn('CardsContainer');

export interface ICardsContainerItem {
    id: string;
    imgSrc: string;
    imgAlt?: string;
    tooltip?: string;
    title: string;
    subtitle?: string;
}

export interface ICardsContainer {
    subtitle: ReactNode;
    items: ICardsContainerItem[];
}

export default class CardsContainer extends Component<ICardsContainer> {
    render() {
        const { items, subtitle } = this.props;
        return (
            <MobileContext.Consumer>
                {({ mobile }) => (
                    <div className={block({ mobile })}>
                        <div className={block('Subtitle')}>{subtitle}</div>
                        <div className={block('Cards')}>
                            {items.map(el => (
                                <Card key={el.id} imgFit {...el} />
                            ))}
                        </div>
                    </div>
                )}
            </MobileContext.Consumer>
        );
    }
}
