import './Button.scss';

import { Component } from 'react';

import { cn } from '@bem-react/classname';

const block = cn('Button');

export default class Button extends Component<{
    onClick: () => void;
    mode: 'accent';
}> {
    render() {
        const { children, onClick, mode } = this.props;
        return (
            <button onClick={onClick} className={block({ mode })}>
                {children}
            </button>
        );
    }
}
