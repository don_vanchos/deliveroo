import './UserBadge.scss';

import { Component } from 'react';

import { ReactComponent as UserIconSvg } from '../../assets/svgs/user-icon.svg';

export default class UserBadge extends Component {
    render() {
        return <UserIconSvg className="UserBadge" />;
    }
}
