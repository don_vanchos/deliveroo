import stubData from '../../assets/stub_data/restaurants.json';

export interface IRestaurant {
    id: string;
    name: string;
    image: string;
    url: string;
    price: number;
    tags: string[];
}

export interface IStubData {
    neighborhood: string;
    restaurants: IRestaurant[];
}

export const simulateBackend = async () => {
    return await new Promise<IStubData>(resolve => {
        const severalMS = Math.floor(Math.random() * 10);
        setTimeout(() => {
            resolve(stubData);
        }, severalMS);
    });
};
