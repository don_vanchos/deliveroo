import './Restaurants.scss';

import { Component } from 'react';

import { cn } from '@bem-react/classname';

import CardsContainer, { ICardsContainerItem } from '../CardsContainer/CardsContainer';
import Location from '../Location/Location';
import { IStubData, simulateBackend } from './RestaurantsStore';

interface IRestaurants {}

const block = cn('Restaurants');

export default class Restaurants extends Component<
    IRestaurants,
    {
        neighborhood: string;
        items: ICardsContainerItem[] | null;
        tagsToLength: Record<string, number>;
        currentTag: string | null;
    }
> {
    private data: IStubData | null = null;

    constructor(props: Readonly<IRestaurants>) {
        super(props);
        this.state = { items: null, tagsToLength: {}, neighborhood: '', currentTag: null };
    }

    componentDidMount() {
        void this.init();
    }

    /**
     * Simulate backend call and get a state.
     * TODO: handle exceptions.
     */
    private async init() {
        // Get data.
        this.data = await simulateBackend();
        // Collect tags.
        const tagsToLength: Record<string, number> = {};
        this.data.restaurants.forEach(el => {
            el.tags.forEach(tag => {
                if (tag in tagsToLength) {
                    tagsToLength[tag]++;
                } else {
                    tagsToLength[tag] = 1;
                }
            });
        });
        // Prepare items to render.
        const items = this.collectItems(null);

        this.setState({ items, tagsToLength, neighborhood: this.data.neighborhood });
    }

    private collectItems(currentTag: string | null) {
        return (
            this.data?.restaurants
                .filter(r => {
                    return currentTag != null ? r.tags.includes(currentTag) : true;
                })
                .map(el => ({
                    id: el.id,
                    imgSrc: el.image,
                    title: el.name,
                    imgAlt: `${el.name} restaurant`,
                    subtitle: [
                        ...el.tags,
                        Array.from({ length: el.price })
                            .map(() => '£')
                            .join(''),
                    ].join(' • '),
                })) ?? []
        );
    }

    onTagClick(currentTag: string | null) {
        const items = this.collectItems(currentTag);
        this.setState({
            currentTag,
            items,
        });
    }

    /**
     * If tag === null we need to render all card.
     */
    renderSubtitleTag(tag: string | null) {
        const name = tag ?? 'View all';
        const len = tag ? this.state.tagsToLength?.[tag] ?? 0 : this.data?.restaurants.length ?? 0;
        return (
            <div
                className={block('SubtitleTag', { current: tag === this.state.currentTag })}
                onClick={() => this.onTagClick(tag)}
                key={tag}
            >
                {`${name} (${len})`}
            </div>
        );
    }

    renderSubtitle() {
        if (this.state.tagsToLength == null || this.data == null) {
            // TODO: here we need the stub.
            return <div>Loading...</div>;
        } else {
            const { tagsToLength } = this.state;
            return (
                <div className={block('Subtitle')}>
                    {[null, ...Object.keys(tagsToLength)].map(tag => this.renderSubtitleTag(tag))}
                </div>
            );
        }
    }

    /**
     * TODO: If there are no restaurants - we should show stubs of cards, not only 'Loading....'
     */
    render() {
        const { items, neighborhood } = this.state;
        return (
            <>
                {items ? (
                    <>
                        <Location title={neighborhood} />
                        <CardsContainer items={items} subtitle={this.renderSubtitle()} />
                    </>
                ) : (
                    <>
                        <Location title="Loading..." />
                        <div>Loading....</div>
                    </>
                )}
            </>
        );
    }
}
