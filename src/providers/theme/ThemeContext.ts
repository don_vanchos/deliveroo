import React from 'react';

export type ThemeType = 'light' | 'dark';

export interface ThemeContextProps {
    theme: ThemeType;
    setTheme: (newTheme: ThemeType) => void;
}

export const initialValue: ThemeContextProps = {
    theme: 'light',
    setTheme: () => {},
};

export const ThemeContext = React.createContext(initialValue);
