import { Component } from 'react';

import { ThemeContext, ThemeContextProps, ThemeType } from './ThemeContext';

export interface ThemeProviderProps {
    theme: ThemeType;
}

interface ThemeProviderState extends ThemeContextProps {}

/**
 * Juggling with style classes.
 * see themes.scss
 */
const updateBodyClassList = (theme: ThemeType) => {
    const classList = document.body.classList;
    // Add all base styles.
    if (!classList.contains('roo-root')) classList.add('roo-root');
    // Include light or dark theme.
    classList.toggle('roo-root_theme_light', theme === 'light');
    classList.toggle('roo-root_theme_dark', theme === 'dark');
};

export class ThemeProvider extends Component<ThemeProviderProps, ThemeProviderState> {
    state: ThemeProviderState = {
        theme: this.props.theme,
        setTheme: (theme) => this.setState({ theme }),
    };

    componentDidMount() {
        updateBodyClassList(this.state.theme);
    }

    /**
     * Update context on changing props or on call setTheme from ThemeContext.
     */
    componentDidUpdate(prevProps: ThemeProviderProps, prevState: ThemeProviderState) {
        let theme: ThemeType | null = null;
        if (prevState.theme !== this.state.theme) {
            theme = this.state.theme;
        }
        if (prevProps.theme !== this.props.theme) {
            theme = this.state.theme;
            this.setState({ theme });
        }
        if (theme) {
            updateBodyClassList(theme);
        }
    }

    render() {
        return <ThemeContext.Provider value={this.state}>{this.props.children}</ThemeContext.Provider>;
    }
}
